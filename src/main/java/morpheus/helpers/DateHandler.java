package morpheus.helpers;

import java.util.Calendar;
import net.minecraft.util.text.TextFormatting;
import morpheus.Morpheus;

public class DateHandler
{
	public static enum Event
	{
		XMAS(25, 12, TextFormatting.RED + "Good morning everyone! MERRY CHRISTMAS!"),
		NEW_YEAR(1, 1, TextFormatting.GOLD + "Good morning everyone! HAPPY NEW YEAR!"),
		STPATRICKS(17, 3, TextFormatting.DARK_GREEN + "Good morning everyone! HAPPY ST PATRICKS DAY!"),
		HALLOWEEN(31, 10, TextFormatting.DARK_PURPLE + "Good morning everyone! HAPPY SAMHAIN!"),
		NONE(0, 0, TextFormatting.GOLD + Morpheus.onMorningText);

		private final int month;
		private final int day;
		private final String text;

		private Event(int day, int month, String text) {
			this.month = month;
			this.day = day;
			this.text = text;
		}

		public boolean isEvent(Calendar calendar) {
			return calendar.get(2) + 1 == this.month && calendar.get(5) == this.day;
		}
	}

	private static Event getEvent(Calendar calendar)
	{
		for (Event event : Event.values())
		{
			if (event.isEvent(calendar))
			{
				return event;
			}
		}
		return Event.NONE;
	}

	public static String getMorningText()
	{
		return DateHandler.getEvent(Calendar.getInstance()).text;
	}
}