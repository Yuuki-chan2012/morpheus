package morpheus.helpers;

import java.util.HashMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import morpheus.Morpheus;
import morpheus.MorpheusRegistry;
import morpheus.api.INewDayHandler;
import morpheus.world.WorldSleepState;

public class SleepChecker
{
	private HashMap<Integer, Boolean> alertSent = new HashMap<Integer, Boolean>();

	public void updatePlayerStates(World world)
	{
		if (world.playerEntities.size() > 1)
		{
			for (EntityPlayer player : world.playerEntities)
			{
				String username = player.getGameProfile().getName();
				if (player.isPlayerFullyAsleep() && !((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(player.dimension.getId()))).isPlayerSleeping(username))
				{
					((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(player.dimension.getId()))).setPlayerAsleep(username);

					alertPlayers(createAlert(player.dimension.getId(), player.getDisplayName().getString(), Morpheus.onSleepText), world);

					continue;
				}
				if (!player.isPlayerFullyAsleep() && ((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(player.dimension.getId()))).isPlayerSleeping(username))
				{
					((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(player.dimension.getId()))).setPlayerAwake(username);

					if (!world.isDaytime() && !((Boolean) this.alertSent.get(Integer.valueOf(world.getDimension().getType().getId()))).booleanValue())
					{
						alertPlayers(createAlert(player.dimension.getId(), player.getDisplayName().getString(), Morpheus.onWakeText), world);
					}
				}
			}
			if (areEnoughPlayersAsleep(world.getDimension().getType().getId()))
			{
				if (!this.alertSent.containsKey(Integer.valueOf(world.getDimension().getType().getId())))
				{
					this.alertSent.put(Integer.valueOf(world.getDimension().getType().getId()), Boolean.valueOf(false));
				}

				advanceToMorning(world);
			}
			else
			{
				this.alertSent.put(Integer.valueOf(world.getDimension().getType().getId()), Boolean.valueOf(false));
			}
		}
	}

	private void alertPlayers(TextComponentString alert, World world)
	{
		if (alert != null && Morpheus.isAlertEnabled())
		{
			for (EntityPlayer player : world.playerEntities)
			{
				player.sendMessage(alert);
			}
		}
	}

	private TextComponentString createAlert(int dimension, String username, String text)
	{
		Morpheus.logger.info(String.format("%s %s %s", new Object[] { username, text, ((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(dimension))).toString() }));

		return new TextComponentString(String.format("%s%s%s %s %s", new Object[] { TextFormatting.WHITE, username, TextFormatting.GOLD, text, ((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(dimension))).toString() }));
	}

	private void advanceToMorning(World world)
	{
		try
		{
			((INewDayHandler) MorpheusRegistry.registry.get(Integer.valueOf(world.getDimension().getType().getId()))).startNewDay();
		}
		catch (Exception e)
		{

			Morpheus.logger.error("Exception caught while starting a new day for dimension " + world.getDimension().getType().getId());
		}

		if (!((Boolean) this.alertSent.get(Integer.valueOf(world.getDimension().getType().getId()))).booleanValue())
		{
			alertPlayers(new TextComponentString(DateHandler.getMorningText()), world);
			((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(world.getDimension().getType().getId()))).wakeAllPlayers();
			this.alertSent.put(Integer.valueOf(world.getDimension().getType().getId()), Boolean.valueOf(true));
		}

		world.getDimension().resetRainAndThunder();
	}

	private boolean areEnoughPlayersAsleep(int dimension)
	{
		if (((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(dimension))).getSleepingPlayers() > 0)
		{
			return ((dimension == 0 || MorpheusRegistry.registry.get(Integer.valueOf(dimension)) != null) && ((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(dimension))).getPercentSleeping() >= Morpheus.perc);
		}

		return false;
	}
}