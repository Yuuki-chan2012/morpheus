package morpheus.helpers;

import net.minecraft.block.BlockBed;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Biomes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import morpheus.Morpheus;
import morpheus.world.WorldSleepState;

public class MorpheusEventHandler
{
	@SubscribeEvent
	public void loggedInEvent(PlayerEvent.PlayerLoggedInEvent event)
	{
		if (!(event.getPlayer().getEntityWorld()).isRemote)
		{
			if (!Morpheus.playerSleepStatus.containsKey(Integer.valueOf((event.getPlayer()).dimension.getId())))
			{
				Morpheus.playerSleepStatus.put(Integer.valueOf((event.getPlayer()).dimension.getId()), new WorldSleepState((event.getPlayer()).dimension.getId()));
			}
			((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf((event.getPlayer()).dimension.getId()))).setPlayerAwake(event.getPlayer().getGameProfile().getName());
		}
	}

	@SubscribeEvent
	public void loggedOutEvent(PlayerEvent.PlayerLoggedOutEvent event)
	{
		if (!(event.getPlayer().getEntityWorld()).isRemote)
		{
			if (Morpheus.playerSleepStatus.get(Integer.valueOf((event.getPlayer()).dimension.getId())) != null)
			{
				((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf((event.getPlayer()).dimension.getId()))).removePlayer(event.getPlayer().getGameProfile().getName());
			}
		}
	}

	@SubscribeEvent
	public void changedDimensionEvent(PlayerEvent.PlayerChangedDimensionEvent event)
	{
		if (!(event.getPlayer().getEntityWorld()).isRemote)
		{
			if (!Morpheus.playerSleepStatus.containsKey(Integer.valueOf(event.getTo().getId())))
			{
				Morpheus.playerSleepStatus.put(Integer.valueOf(event.getTo().getId()), new WorldSleepState(event.getTo().getId()));
			}

			if (Morpheus.playerSleepStatus.get(Integer.valueOf(event.getFrom().getId())) != null)
			{
				((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(event.getFrom().getId()))).removePlayer(event.getPlayer().getGameProfile().getName());
			}

			((WorldSleepState) Morpheus.playerSleepStatus.get(Integer.valueOf(event.getTo().getId()))).setPlayerAwake(event.getPlayer().getGameProfile().getName());
		}
	}

	@SubscribeEvent
	public void worldTickEvent(TickEvent.WorldTickEvent event)
	{
		if (!event.world.isRemote)
		{
			if (event.world.getGameTime() % 20L == 10L && event.phase == TickEvent.Phase.END)
			{
				if (event.world.playerEntities.size() > 0)
				{
					if (!Morpheus.playerSleepStatus.containsKey(Integer.valueOf(event.world.getDimension().getType().getId())))
					{
						Morpheus.playerSleepStatus.put(Integer.valueOf(event.world.getDimension().getType().getId()), new WorldSleepState(event.world.getDimension().getType().getId()));
					}

					Morpheus.checker.updatePlayerStates(event.world);
				}
				else
				{
					Morpheus.playerSleepStatus.remove(Integer.valueOf(event.world.getDimension().getType().getId()));
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void bedClicked(PlayerInteractEvent.RightClickBlock event)
	{
		if (Morpheus.setSpawnDaytime)
		{
			EntityPlayer player = event.getEntityPlayer();
			BlockPos pos = event.getPos();

			if (!(event.getWorld()).isRemote && event.getWorld().isDaytime() && !event.getEntityPlayer().isSneaking())
			{
				if (player.getBedLocation(player.dimension) == null || player.getBedLocation(player.dimension).getDistance(pos.getX(), pos.getY(), pos.getZ()) > 4.0D)
				{
					IBlockState state = event.getWorld().getBlockState(pos);
					if (state.getBlock() instanceof BlockBed)
					{
						if (event.getWorld().getDimension().canRespawnHere() && event.getWorld().getDimension().getBiome(pos) != Biomes.NETHER)
						{
							if (!((Boolean) state.get(BlockBed.OCCUPIED)).booleanValue())
							{
								player.setSpawnPoint(pos, false, event.getWorld().getDimension().getType());
								player.sendMessage(new TextComponentString("New spawnpoint has been set!"));
								event.setCanceled(true);
							}
						}
					}
				}
			}
		}
	}
}