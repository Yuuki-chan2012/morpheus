package morpheus;

import java.util.HashMap;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import morpheus.commands.CommandMorpheus;
import morpheus.helpers.Config;
import morpheus.helpers.MorpheusEventHandler;
import morpheus.helpers.SleepChecker;
import morpheus.world.WorldSleepState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod("morpheus")
public class Morpheus
{
	public static final String MODID = "morpheus";
	public static Morpheus instance;
	public static Logger logger = LogManager.getLogger("morpheus");
	public static int perc;
	public static String onSleepText;
	public static String onWakeText;
	public static final HashMap<Integer, WorldSleepState> playerSleepStatus = new HashMap<Integer, WorldSleepState>();
	public static final SleepChecker checker = new SleepChecker();
	public static MorpheusRegistry register = new MorpheusRegistry();

	public static String onMorningText;

	private static boolean alertEnabled;

	public Morpheus()
	{
		instance = this;
		MinecraftForge.EVENT_BUS.register(this);
		MinecraftForge.EVENT_BUS.register(new MorpheusEventHandler());
		ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, Config.SERVER_SPEC);
	}

	public static boolean includeMiners;

	public static boolean isAlertEnabled()
	{
		return alertEnabled;
	}

	public static int groundLevel;
	public static boolean setSpawnDaytime;

	public static void setAlertPlayers(boolean state)
	{
		alertEnabled = state;
	}

	@SubscribeEvent
	public void serverLoad(FMLServerStartingEvent event)
	{
		CommandMorpheus.register(event.getCommandDispatcher());
	}
}