package morpheus.api;

public interface INewDayHandler
{
	void startNewDay();
}