package morpheus.api;

public interface IMorpheusAPI
{
	void registerHandler(INewDayHandler paramINewDayHandler, int paramInt);
	void unregisterHandler(int paramInt);
}