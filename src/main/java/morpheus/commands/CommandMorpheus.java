package morpheus.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import morpheus.Morpheus;
import morpheus.helpers.Config;

public class CommandMorpheus
{
	public static void register(CommandDispatcher<CommandSource> cmdDisp)
	{
		LiteralArgumentBuilder<CommandSource> morpheusCommand = Commands.literal("wallet");

		morpheusCommand.executes(command ->
		{
			((CommandSource) command.getSource()).sendFeedback((new TextComponentString("Usage: /morpheus <alert : version> | /morpheus percent <percentage> | /morpheus disable <dimension number>")).setStyle((new Style()).setColor(TextFormatting.RED)), true);

			return 1;
		});

		morpheusCommand.then(Commands.literal("version").executes(command ->
		{
			((CommandSource) command.getSource()).sendFeedback(new TextComponentString("Morpheus version: 1.13.2-4.0.-custom"), true);

			return 1;
		}));

		morpheusCommand.then(Commands.literal("alert").executes(command ->
		{
			if (Morpheus.isAlertEnabled())
			{
				Morpheus.setAlertPlayers(false);

				((CommandSource) command.getSource()).sendFeedback(new TextComponentString("Text alerts turned off"), true);
			}
			else
			{
				Morpheus.setAlertPlayers(true);

				((CommandSource) command.getSource()).sendFeedback(new TextComponentString("Text alerts turned on"), true);
			}
			return 1;
		}));

		((LiteralArgumentBuilder) morpheusCommand.then(Commands.literal("percent"))).then(Commands.argument("value", IntegerArgumentType.integer(0, 100)).executes(command ->
				{
					if (((CommandSource) command.getSource()).hasPermissionLevel(2))
					{
						int newPercent = ((Integer) command.getArgument("value", Integer.class)).intValue();
						Morpheus.perc = newPercent;
						Config.perc = newPercent;

						((CommandSource) command.getSource()).sendFeedback(new TextComponentString("Sleep vote percentage set to " + Morpheus.perc + "%"), true);
					}
					return 1;
				}));

		morpheusCommand.then(((LiteralArgumentBuilder) Commands.literal("disable").then(Commands.argument("dim", IntegerArgumentType.integer()))).executes(command ->
				{
					if (((CommandSource) command.getSource()).hasPermissionLevel(2))
					{
						int ageToDisable = ((Integer) command.getArgument("dim", Integer.class)).intValue();
						if (Morpheus.register.isDimRegistered(ageToDisable))
						{
							Morpheus.register.unregisterHandler(ageToDisable);

							((CommandSource) command.getSource()).sendFeedback(new TextComponentString("Disabled sleep vote checks in dimension " + ageToDisable), true);
						}
						else
						{
							((CommandSource) command.getSource()).sendFeedback(new TextComponentString("Sleep vote checks are already disabled in dimension " + ageToDisable), true);
						}
					}
					return 1;
				}));

		cmdDisp.register(morpheusCommand);
	}
}