package morpheus.world;

import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import morpheus.api.INewDayHandler;

public class DefaultOverworldHandler implements INewDayHandler
{
	public void startNewDay()
	{
		WorldServer worldServer = ServerLifecycleHooks.getCurrentServer().getWorld(DimensionType.OVERWORLD);
		worldServer.setGameTime(worldServer.getGameTime() + getTimeToSunrise(worldServer));
	}

	private long getTimeToSunrise(World world)
	{
		long dayLength = 24000L;
		return dayLength - world.getGameTime() % dayLength;
	}
}