package morpheus.world;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import morpheus.Morpheus;

public class WorldSleepState
{
	private int dimension;
	private HashMap<String, Boolean> playerStatus;

	public WorldSleepState(int dimension)
	{
		this.dimension = dimension;
		this.playerStatus = new HashMap<String, Boolean>();
	}

	public int getPercentSleeping()
	{
		return (this.playerStatus.size() - getMiningPlayers() > 0) ? ((getSleepingPlayers() > 0) ? (getSleepingPlayers() * 100 / (this.playerStatus.size() - getMiningPlayers())) : 0) : 100;
	}

	private int getMiningPlayers()
	{
		int miningPlayers = 0;
		for (EntityPlayer player : (ServerLifecycleHooks.getCurrentServer().getWorld(DimensionType.getById(this.dimension))).playerEntities)
		{
			if (player.posY < Morpheus.groundLevel)
			{
				miningPlayers++;
			}
		}
		return !Morpheus.includeMiners ? miningPlayers : 0;
	}

	public int getSleepingPlayers()
	{
		int asleepCount = 0;
		for (Map.Entry<String, Boolean> entry : this.playerStatus.entrySet())
		{
			if (((Boolean) entry.getValue()).booleanValue())
			{
				asleepCount++;
			}
		}
		return asleepCount;
	}

	public String toString()
	{
		return Morpheus.includeMiners ? (getSleepingPlayers() + "/" + this.playerStatus.size() + " (" + getPercentSleeping() + "%)") : (getSleepingPlayers() + "/" + this.playerStatus.size() + " - " + getMiningPlayers() + " miners (" + getPercentSleeping() + "%)");
	}

	public void setPlayerAsleep(String username)
	{
		this.playerStatus.put(username, Boolean.valueOf(true));
	}

	public void setPlayerAwake(String username)
	{
		this.playerStatus.put(username, Boolean.valueOf(false));
	}

	public boolean isPlayerSleeping(String username)
	{
		if (this.playerStatus.containsKey(username))
		{
			return ((Boolean) this.playerStatus.get(username)).booleanValue();
		}

		this.playerStatus.put(username, Boolean.valueOf(false));

		return false;
	}

	public void removePlayer(String username)
	{
		this.playerStatus.remove(username);
	}

	public void wakeAllPlayers()
	{
		for (Map.Entry<String, Boolean> entry : this.playerStatus.entrySet())
		{
			entry.setValue(Boolean.valueOf(false));
		}
	}
}