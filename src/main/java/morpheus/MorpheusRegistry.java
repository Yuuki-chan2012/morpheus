package morpheus;

import java.util.HashMap;
import morpheus.api.IMorpheusAPI;
import morpheus.api.INewDayHandler;
import morpheus.world.DefaultOverworldHandler;

public class MorpheusRegistry implements IMorpheusAPI
{
	public static HashMap<Integer, INewDayHandler> registry;

	public MorpheusRegistry()
	{
		registry = new HashMap<Integer, INewDayHandler>();
		registerHandler(new DefaultOverworldHandler(), 0);
	}

	public void registerHandler(INewDayHandler newDayHandler, int dimension)
	{
		if (registry.containsKey(Integer.valueOf(dimension)))
			Morpheus.logger.warn("New day handler for dimension " + dimension + " has been replaced");

		registry.put(Integer.valueOf(dimension), newDayHandler);
	}

	public void unregisterHandler(int dimension)
	{
		registry.remove(Integer.valueOf(dimension));
	}

	public boolean isDimRegistered(int dim)
	{
		return registry.containsKey(Integer.valueOf(dim));
	}
}