Open Sourced version of Quetzi's "Morpheus" mod.

What I did:

- Decompiled with jd-gui and CFR (http://www.javadecompilers.com/)

- Removed "net.quetzi." package is this is unnecessary in this code.

- Removed unused imports and fixed a few HashMap warnings (pls Quetzi, you know better than this...)

- Updated Forge to 1.13.2v25.0.209 and mappings 20190516-1.13.2
